/**
 * 
 */
package org.mattspoon.spaceshooter.screens;

import org.mattspoon.spaceshooter.SpaceShooter;
import org.mattspoon.spaceshooter.systems.InputSystem;
import org.mattspoon.spaceshooter.systems.MovementSystem;
import org.mattspoon.spaceshooter.systems.RenderSystem;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

/**
 * @author mspoon
 *
 */
public class GameScreen implements Screen {
	SpaceShooter game;
	
	RenderSystem renderSystem;
	MovementSystem movementSystem;
	InputSystem inputSystem;
	
	private float enemyGenCounter;
	
	public GameScreen(SpaceShooter game) {
		this.game = game;
		
		// Add systems
		movementSystem = game.world.setSystem(new MovementSystem());
		inputSystem = game.world.setSystem(new InputSystem(game));
		renderSystem = game.world.setSystem(new RenderSystem(game.camera), true);
		
		game.world.initialize();
		
		// Add Entities
		game.entityFactory.createPlayer(25, SpaceShooter.HEIGHT / 2);
		
		enemyGenCounter = 0.0f;
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#render(float)
	 */
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		//Check to see if I should create an enemy
		enemyGenCounter += delta;
		
		if (enemyGenCounter > 1.0f) {
			enemyGenCounter = 0.0f;
			game.entityFactory.createEnemy();
		}
		
		game.world.setDelta(delta);
		game.world.process();
		
		renderSystem.process();
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#resize(int, int)
	 */
	@Override
	public void resize(int width, int height) {
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#show()
	 */
	@Override
	public void show() {
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#hide()
	 */
	@Override
	public void hide() {
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#pause()
	 */
	@Override
	public void pause() {
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#resume()
	 */
	@Override
	public void resume() {
	}

	/* (non-Javadoc)
	 * @see com.badlogic.gdx.Screen#dispose()
	 */
	@Override
	public void dispose() {
	}

}
