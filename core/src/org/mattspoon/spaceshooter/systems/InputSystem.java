package org.mattspoon.spaceshooter.systems;

import org.mattspoon.spaceshooter.SpaceShooter;
import org.mattspoon.spaceshooter.components.Movable;
import org.mattspoon.spaceshooter.components.Player;
import org.mattspoon.spaceshooter.components.Position;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class InputSystem extends EntityProcessingSystem {
	@Mapper
	ComponentMapper<Player> pm;
	@Mapper
	ComponentMapper<Movable> mm;
	@Mapper
	ComponentMapper<Position> posm;
	
	SpaceShooter game;
	
	float bulletRate;
	float timeSinceLastBullet;

	@SuppressWarnings("unchecked")
	public InputSystem(SpaceShooter game) {
		super(Aspect.getAspectForAll(Player.class, Movable.class, Position.class));
		this.game = game;
		
		bulletRate = 0.1f;
		timeSinceLastBullet = 0.0f;
	}

	@Override
	protected void process(Entity e) {
		Movable move = mm.get(e);
		Position pos = posm.get(e);
		
		timeSinceLastBullet += world.getDelta();
		
		if (Gdx.input.isKeyPressed(Keys.UP)) {
			move.accel.y = 256.0f;
		} else if (Gdx.input.isKeyPressed(Keys.DOWN)) {
			move.accel.y = -256.0f;
		} else {
			move.accel.y = 0.0f;
		}
		
		if (Gdx.input.isKeyPressed(Keys.SPACE) && timeSinceLastBullet > bulletRate) {
			float x = pos.x + 40.0f;
			float y = pos.y + 20.0f;
			timeSinceLastBullet = 0.0f;
			
			game.entityFactory.createBullet(x, y, 256.0f);
		}
	}

}
