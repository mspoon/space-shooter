/**
 * 
 */
package org.mattspoon.spaceshooter.systems;

import org.mattspoon.spaceshooter.components.Movable;
import org.mattspoon.spaceshooter.components.Position;
import org.mattspoon.spaceshooter.components.Sprite;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.Gdx;

/**
 * @author mspoon
 *
 */
public class MovementSystem extends EntityProcessingSystem {
	@Mapper
	ComponentMapper<Movable> mm;
	@Mapper
	ComponentMapper<Position> pm;
	@Mapper
	ComponentMapper<Sprite> sm;
	
	private float globalSpeedLimit = 384.0f;

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public MovementSystem() {
		super(Aspect.getAspectForAll(Movable.class, Position.class, Sprite.class));
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see com.artemis.systems.EntityProcessingSystem#process(com.artemis.Entity)
	 */
	@Override
	protected void process(Entity e) {
		Movable move = mm.get(e);
		Position pos = pm.get(e);
		Sprite sprite = sm.get(e);
		
		// Get delta (I believe delta is in seconds, which means I should be able to just multiply)
		float delta = world.getDelta();
		
		// Process acceleration first
		
		if (move.velocity.x <= globalSpeedLimit)
			move.velocity.x += (move.accel.x * delta);
		if (move.velocity.y <= globalSpeedLimit)
			move.velocity.y += (move.accel.y * delta);
		
		// Now move it
		pos.x += move.velocity.x * delta;
		pos.y += move.velocity.y * delta;
		
		// For now, make sure it stays on screen here
		int screenHeight = Gdx.graphics.getHeight();
		if (Math.round(pos.y) < 0) {
			move.velocity.y = 0.0f;
			move.accel.y = 0.0f;
			pos.y = 0.0f;
		} else if (Math.round(pos.y) + sprite.height > screenHeight) {
			move.velocity.y = 0.0f;
			move.accel.y = 0.0f;
			pos.y = screenHeight - sprite.height;
		}
		
		if ((pos.x < 0.0f) || (Math.round(pos.x) > Gdx.graphics.getWidth())) {
			e.deleteFromWorld();
		}
	}

}
