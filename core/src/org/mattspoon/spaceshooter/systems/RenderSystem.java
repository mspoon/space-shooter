/**
 * 
 */
package org.mattspoon.spaceshooter.systems;

import org.mattspoon.spaceshooter.components.Position;
import org.mattspoon.spaceshooter.components.Sprite;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.EntitySystem;
import com.artemis.annotations.Mapper;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * @author mspoon
 *
 */
public class RenderSystem extends EntitySystem {
	@Mapper
	private ComponentMapper<Position> pm;
	@Mapper
	private ComponentMapper<Sprite> sm;
	
	private OrthographicCamera camera;
	private SpriteBatch batch;

	@SuppressWarnings("unchecked")
	public RenderSystem(OrthographicCamera camera) {
		super(Aspect.getAspectForAll(Position.class, Sprite.class));
		this.camera = camera;
		
		batch = new SpriteBatch();
	}

	/* (non-Javadoc)
	 * @see com.artemis.EntitySystem#processEntities(com.artemis.utils.ImmutableBag)
	 */
	@Override
	protected void processEntities(ImmutableBag<Entity> entities) {
		for (int i=0; i < entities.size(); i++) {
			process(entities.get(i));
		}
	}
	
	protected void process(Entity e) {
		if (pm.has(e)) {
			Position pos = pm.get(e);
			Sprite sprite = sm.get(e);
			batch.draw(sprite.image, pos.x, pos.y, sprite.width, sprite.height);
		}
	}

	/* (non-Javadoc)
	 * @see com.artemis.EntitySystem#checkProcessing()
	 */
	@Override
	protected boolean checkProcessing() {
		return true;
	}

	@Override
	protected void begin() {
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
	}

	@Override
	protected void end() {
		batch.end();
	}

}
