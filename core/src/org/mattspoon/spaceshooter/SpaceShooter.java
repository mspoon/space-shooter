package org.mattspoon.spaceshooter;

import org.mattspoon.spaceshooter.screens.GameScreen;

import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.OrthographicCamera;


public class SpaceShooter extends Game {
	public World world;
	public OrthographicCamera camera;
	public EntityFactory entityFactory;
	
	public static final int WIDTH = 800;
	public static final int HEIGHT = 480;
	
	@Override
	public void create() {
		world = new World();
		world.setManager(new GroupManager());
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, SpaceShooter.WIDTH, SpaceShooter.HEIGHT);
		camera.update();
		
		entityFactory = new EntityFactory(world);
		
		setScreen(new GameScreen(this));
	}
}

