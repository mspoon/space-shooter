/**
 * 
 */
package org.mattspoon.spaceshooter;

import org.mattspoon.spaceshooter.components.Collider;
import org.mattspoon.spaceshooter.components.Movable;
import org.mattspoon.spaceshooter.components.Player;
import org.mattspoon.spaceshooter.components.Position;
import org.mattspoon.spaceshooter.components.Sprite;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;

/**
 * @author mspoon
 *
 */
public class EntityFactory {
	private World world;
	private Texture playerSprite;
	private Texture enemySprite;
	private Texture bulletSprite;
	
	public EntityFactory(World world) {
		this.world = world;
		playerSprite = new Texture(Gdx.files.internal("playerShip.png"));
		enemySprite = new Texture(Gdx.files.internal("DurrrSpaceShip.png"));
		
		Pixmap bulletPixmap = new Pixmap(5, 5, Format.RGBA4444);
		bulletPixmap.setColor(Color.WHITE);
		bulletPixmap.fill();
		bulletSprite = new Texture(bulletPixmap);
	}
	
	public Entity createPlayer(float x, float y) {
		Entity e = world.createEntity();
		
		e.addComponent(new Position(x, y));
		e.addComponent(new Sprite(playerSprite, 40.0f, 40.0f));
		e.addComponent(new Movable(0.0f, 0.0f));
		e.addComponent(new Player());
		//e.addComponent(new Collider(x, y, 40.0f, 40.0f));
		
		world.addEntity(e);
		
		return e;
	}
	
	public Entity createEnemy() {
		Entity e = world.createEntity();
		
		float newX = Gdx.graphics.getWidth();
		float newY = MathUtils.random(Gdx.graphics.getHeight() - 40.0f);
		e.addComponent(new Position(newX, newY));
		e.addComponent(new Sprite(enemySprite, 40.0f, 40.0f));
		e.addComponent(new Movable(-64.0f, 0.0f, -112.0f, 0.0f));
		//e.addComponent(new Collider(newX, newY, 40.0f, 40.0f));
		
		world.addEntity(e);
		
		return e;
	}
	
	public Entity createBullet(float x, float y, float vel_x) {
		Entity e = world.createEntity();
		
		e.addComponent(new Position(x, y));
		e.addComponent(new Sprite(bulletSprite, 5.0f, 5.0f));
		e.addComponent(new Movable(vel_x, 0.0f, 0.0f, 0.0f));
		//e.addComponent(new Collider(x, y, 5.0f, 5.0f));
		
		world.addEntity(e);
		
		return e;
	}
}
