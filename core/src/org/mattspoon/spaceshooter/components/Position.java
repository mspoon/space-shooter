/**
 * 
 */
package org.mattspoon.spaceshooter.components;

import com.artemis.Component;
import com.badlogic.gdx.math.Vector2;

/**
 * @author mspoon
 *
 */
public class Position extends Component {
	public float x, y;
	
	public Position(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	public Vector2 toVector2() {
		return new Vector2(x, y);
	}
}
