/**
 * 
 */
package org.mattspoon.spaceshooter.components;

import com.artemis.Component;
import com.badlogic.gdx.math.Vector2;

/**
 * @author mspoon
 *
 */
public class Movable extends Component {
	public Vector2 velocity;
	public Vector2 accel;
	
	public Movable(float vel_x, float vel_y, float accel_x, float accel_y) {
		velocity = new Vector2(vel_x, vel_y);
		accel = new Vector2(accel_x, accel_y);
	}
	
	public Movable(float vel_x, float vel_y) {
		velocity = new Vector2(vel_x, vel_y);
		accel = new Vector2(0.0f, 0.0f);
	}
	
	public Movable(Vector2 vel, Vector2 accel) {
		this.velocity = vel;
		this.accel = accel;
	}
	
	public Movable(Vector2 vel) {
		this.velocity = vel;
		accel = new Vector2(0.0f, 0.0f);
	}
}
