/**
 * 
 */
package org.mattspoon.spaceshooter.components;

import com.artemis.Component;
import com.badlogic.gdx.graphics.Texture;

/**
 * @author mspoon
 *
 */
public class Sprite extends Component {
	public Texture image;
	public float width,height;
	
	public Sprite(Texture img) {
		image = img;
		width = (float)image.getWidth();
		height = (float)image.getHeight();
	}
	
	public Sprite(Texture img, float width, float height) {
		image = img;
		this.width = width;
		this.height = height;
	}
}
