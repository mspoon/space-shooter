package org.mattspoon.spaceshooter.components;

import com.artemis.Component;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Collider extends Component {
	public Rectangle AABB;

	public Collider(Vector2 pos, Vector2 size) {
		AABB.x = pos.x;
		AABB.y = pos.y;
		AABB.width = size.x;
		AABB.height = size.y;
	}
	
	public Collider(float x, float y, float w, float h) {
		AABB.x = x;
		AABB.y = y;
		AABB.width = w;
		AABB.height = h;
	}

}
